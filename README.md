# docker-compose

一些自用的docker compose文件

## 安装docker和docker compose

- [ ] [Get Docker](https://get.docker.com/)
- [ ] [Get Compose](https://docs.docker.com/compose/install/)

```
# 安装docker
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh
# 安装compose
curl -SL https://github.com/docker/compose/releases/download/v2.5.0/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
```
